# I. Setup base de données
## 1. Install MariaDB

### Installer MariaDB sur la machine ```db.tp2.cesi```

```bash
[rockstt@db ~]$ sudo dnf install mariadb-server
[...]
  perl-parent-1:0.237-1.el8.noarch                                                 perl-podlators-4.11-1.el8.noarch
  perl-threads-1:2.21-2.el8.x86_64                                                 perl-threads-shared-1.58-2.el8.x86_64
  psmisc-23.1-5.el8.x86_64

Complete!
```

### Le service MariaDB
```bash
[rockstt@db ~]$ sudo systemctl enable mariadb && systemctl status mariadb.service
[sudo] password for rockstt:
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.

● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: inactive (dead)
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
           
[rockstt@db ~]$ sudo systemctl start mariadb.service && systemctl status mariadb.service
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-14 15:58:44 CET; 13s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
  Process: 5268 ExecStartPost=/usr/libexec/mysql-check-upgrade (code=exited, status=0/SUCCESS)
  Process: 5133 ExecStartPre=/usr/libexec/mysql-prepare-db-dir mariadb.service (code=exited, status=0/SUCCESS)
  Process: 5107 ExecStartPre=/usr/libexec/mysql-check-socket (code=exited, status=0/SUCCESS)
 Main PID: 5237 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 11235)
   Memory: 83.7M
[...]
Dec 14 15:58:44 db.tp2.cesi systemd[1]: Started MariaDB 10.3 database server.

[rockstt@db ~]$ sudo ss -lutpn
Netid       State        Recv-Q       Send-Q              Local Address:Port               Peer Address:Port       Process
udp         UNCONN       0            0                       127.0.0.1:323                     0.0.0.0:*           users:(("chronyd",pid=999,fd=6))
udp         UNCONN       0            0                           [::1]:323                        [::]:*           users:(("chronyd",pid=999,fd=7))
tcp         LISTEN       0            128                       0.0.0.0:22                      0.0.0.0:*           users:(("sshd",pid=1048,fd=5))
tcp         LISTEN       0            128                             *:9090                          *:*           users:(("systemd",pid=1,fd=87))
tcp         LISTEN       0            80                              *:3306                          *:*           users:(("mysqld",pid=5237,fd=21))
tcp         LISTEN       0            128                          [::]:22                         [::]:*           users:(("sshd",pid=1048,fd=7))

[rockstt@db ~]$ ps -ef
mysql       5237       1  0 15:58 ?        00:00:00 /usr/libexec/mysqld --basedir=/usr
root        5334       2  0 16:01 ?        00:00:00 [kworker/2:1-events_power_efficient]
rockstt     5355    1872  0 16:04 pts/0    00:00:00 ps -ef
```

### Firewall

```bash
[rockstt@db ~]$ sudo firewall-cmd --permanent --add-port=3306/tcp
success

[rockstt@db ~]$ sudo firewall-cmd --reload
[sudo] password for rockstt:
success
```

## 2. Conf MariaDB

### Configuration élémentaire de la base

```bash
[rockstt@db ~]$ sudo mysql_secure_installation
``` 
```SQL
NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!
[...]
Set root Password? [Y/n] Y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!
 
Remove anonymous users? [Y/n] Y
 ... Success!

Disallow root login remotely? [Y/n] Y
 ... Success!
 
 Remove test database and access to it? [Y/n] n
 ... skipping.
 
 Reload privilege tables now? [Y/n] Y
 ... Success!
 
 Cleaning up...

"All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure."

Thanks for using MariaDB!
```

### Préparation de la base en vue de l'utilisation par NextCloud

```bash
[rockstt@db ~]$ sudo mysql -u root -p
[sudo] password for rockstt: 
Enter password:
Welcome to the MariaDB monitor. Commends end with ; or \g.
[...]
```

```sql
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.2.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';

Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```

## 3. TEST

### Installez sur la machine ```web.tp2.cesi``` la commande ```mysql```

```bash
[rockstt@web ~]$ sudo dnf provides mysql
Last metadata expiration check: 1:03:01 ago on Wed 15 Dec 2021 09:01:57 AM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs
                                                  : and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

[rockstt@web ~]$ sudo dnf install mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64

Last metadata expiration check: 1:03:44 ago on Wed 15 Dec 2021 09:01:57 AM CET.
Dependencies resolved.
[...]
Installed:
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch
  mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
  mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64

Complete!
```

### Tester la connexion
```bash
[rockstt@web ~]$ mysql -h 10.2.1.12 --port 3306 -u nextcloud -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 21
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server
```

```sql
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.01 sec)

mysql> use nextcloud;
Database changed
mysql> show tables;
Empty set (0.00 sec)
```

# II.Setup Apache

## 1. Install Apache

## A. Apache

### Installer Apache sur la machine```web.tp2.cesi```

```bash
[rockstt@web ~]$ sudo dnf install httpd
[...]
Installed:
  apr-1.6.3-12.el8.x86_64
  apr-util-1.6.1-6.el8.1.x86_64
  apr-util-bdb-1.6.1-6.el8.1.x86_64
  apr-util-openssl-1.6.1-6.el8.1.x86_64
  httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch
  httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64
  rocky-logos-httpd-85.0-3.el8.noarch

Complete!
```

### Analyse du service Apache

```bash
[rockstt@web ~]$ sudo systemctl start httpd
[rockstt@web ~]$ sudo systemctl enable httpd
[sudo] password for rockstt:
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.

[rockstt@web ~]$ ps -ef
[...]
root        3026       1  0 11:26 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      3027    3026  0 11:26 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      3028    3026  0 11:26 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      3029    3026  0 11:26 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      3030    3026  0 11:26 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
[...]

[rockstt@web ~]$ sudo ss -lutpn
[sudo] password for rockstt:
Netid       State        Recv-Q       Send-Q              Local Address:Port               Peer Address:Port       Process
udp         UNCONN       0            0                       127.0.0.1:323                     0.0.0.0:*           users:(("chronyd",pid=1013,fd=6))
udp         UNCONN       0            0                           [::1]:323                        [::]:*           users:(("chronyd",pid=1013,fd=7))
tcp         LISTEN       0            128                       0.0.0.0:22                      0.0.0.0:*           users:(("sshd",pid=1059,fd=5))
tcp         LISTEN       0            128                          [::]:22                         [::]:*           users:(("sshd",pid=1059,fd=7))
tcp         LISTEN       0            128                             *:9090                          *:*           users:(("systemd",pid=1,fd=27))
tcp         LISTEN       0            128                             *:80                            *:*           users:(("httpd",pid=3030,fd=4),("httpd",pid=3029,fd=4),("httpd",pid=3028,fd=4),("httpd",pid=3026,fd=4))
```

### Un premier test

```bash
[rockstt@web ~]$ sudo firewall-cmd --permanent --add-port=80/tcp
success

[rockstt@web ~]$ sudo firewall-cmd --reload
success

[rockstt@web ~]$ curl 127.0.0.1:80
[...]

  </body>
</html>
```

## B.PHP

###  Installer PHP
```bash
[rockstt@web ~]$ sudo dnf install epel-release
[...]
Installed:
  epel-release-8-13.el8.noarch

Complete!

[rockstt@web ~]$ sudo dnf update
Extra Packages for Enterprise Linux 8 - x86 3.1 MB/s |  11 MB     00:03
Extra Packages for Enterprise Linux Modular 728 kB/s | 980 kB     00:01
Last metadata expiration check: 0:00:01 ago on Wed 15 Dec 2021 01:51:12 PM CET.
Dependencies resolved.
Nothing to do.
Complete!

[rockstt@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
[...]
Installed:
  remi-release-8.5-2.el8.remi.noarch

Complete!

[rockstt@web ~]$ sudo dnf module enable php:remi-7.4
[...]
============================================================================
 Package          Architecture    Version            Repository        Size
============================================================================
Enabling module streams:
 php                              remi-7.4

Transaction Summary
============================================================================

Is this ok [y/N]: y
Complete!

sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
[...]
  tcl-1:8.6.8-2.el8.x86_64

Complete!
```

## 2. Conf apache

### Analyser la conf Apache

```bash
[rockstt@web ~]$ nano /etc/httpd/conf/httpd.conf
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf
```

### Créer un VirtualHost qui accueillera NextCloud

```bash
[rockstt@web conf.d]$ sudo touch VirtualhostNC.conf
[rockstt@web conf.d]$ sudo nano VirtualhostNC.conf
[Contenu TP]

[rockstt@web conf.d]$ sudo systemctl restart httpd
```

### Configurer la racine Web

```bash
[rockstt@web www]$ sudo mkdir -p ./nextcloud/html

[rockstt@web www]$ sudo chown -R apache:apache nextcloud/

drwxr-xr-x. 3 apache apache 18 Dec 15 14:50 nextcloud
```

### Configurer PHP

```bash
[rockstt@web www]$ timedatectl
               Local time: Wed 2021-12-15 14:56:12 CET
           Universal time: Wed 2021-12-15 13:56:12 UTC
                 RTC time: Wed 2021-12-15 13:56:12
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no

[rockstt@web www]$ sudo nano /etc/opt/remi/php74/php.ini
;date.timezone = "Europe/Paris (CET, +0100)"
```

# III. NextCloud

### Récupérer Nextcloud

```bash
[rockstt@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
100  148M  100  148M    0     0  1206k      0  0:02:05  0:02:05 --:--:-- 112100  148M  100  148M    0     0  1206k      0  0:02:05  0:02:05 --:--:-- 1108k
```

### Ranger la chambre

```bash
[rockstt@web ~]$ unzip nextcloud-21.0.1.zip
 extracting: nextcloud/config/CAN_INSTALL
  inflating: nextcloud/config/config.sample.php
  inflating: nextcloud/config/.htaccess
  
[rockstt@web ~]$ ls
nextcloud  nextcloud-21.0.1.zip
[rockstt@web ~]$ rm nextcloud-21.0.1.zip
[rockstt@web nextcloud]$ cp -R ./* /var/www/nextcloud/html/
[rockstt@web html]$ sudo chown apache:apache /var/www/nextcloud/html/*
```

